//날짜 생성 함수(YYYYMMDD)
const getYmd = (mode) => {
  var date = new Date();
  var year = date.getFullYear();
  var month = ("0" + (1 + date.getMonth())).slice(-2);
  var day = ("0" + date.getDate()).slice(-2);
  
  if (mode == "dash") {
    return year + "-" + month + "-" + day;
  } else if (mode=="korean") {
    return year + "년 " + month + "월 " + day + "일";
  } else {
    return year + month + day;
  }
}

module.exports = getYmd;