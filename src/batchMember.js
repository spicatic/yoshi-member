const cors = require('cors');
const express = require("express");
const app = express();
const axios = require("axios");
const cheerio = require("cheerio");
const fs = require("fs");
const path = require("path");
const schedule = require("node-schedule");

const getYmd = require("./getYmd.js");

const directoryJsonMember = "./public/json/member/"
const directoryJsonMap = "./public/json/map/map.json"

//지정한 길드와 페이지의 HTML을 return
const getHtml = async (gid, wid, orderby, page) => {
  try {
    //작성일 기준 2022.04.이후 5년 내로 url 변경될 여지 있음
    return await axios.get(`https://maplestory.nexon.com/Common/Guild?gid=${gid}&wid=${wid}&orderby=${orderby}&page=${page}`);
  } catch(error) {
    console.error(error);
  }
};

const writeJsonMember = () => {
    //페이지 갯수 return 함수
  const setPage = async () =>{
    let html = await getHtml(285951, 1, 0, 1);
    const $ = cheerio.load(html.data);
    return $(".page_numb").children("a").length
  };
  setPage().then( pageMax => {
    //setPage의 결과가 0일 경우 웹페이지 점검이거나 오류가 발생한 것으로 처리하여 스크립트 실행하지 않음
    if ( pageMax > 0 ) {
      //arrName에 모든 길드원 이름을 배열로 채우는 함수
      var setArrNameMember = async () => {
        let arrNameMember = [];
        for (var a = 1; a <= pageMax; a++) {
          let html = await getHtml(285951, 1, 0, a);
          const $ = cheerio.load(html.data);
          for (var b = 0; b<$(".rank_table tr").length-1; b++) {
            arrNameMember.push($(".rank_table .left a").eq(b).text());
          }
        }
        return arrNameMember;
      }
      //파일이름, 생성시간 포함된 객체로 구성된 배열 생성
      var arrObjMember = fs.readdirSync(directoryJsonMember).map(nameFile => {
        return {
          nameFile: nameFile,
          timeCreated: fs.statSync(directoryJsonMember + nameFile).mtime
        }
      });
      
      if (arrObjMember.length == 0){
        //이번 함수 실행이 처음일 경우(기존 파일이 없을 경우 모든 배열을 빈 배열로 설정하여 오류 방지 및 최적화)
        var arrObjMember = [];
        var listMemberLastest = [];
        var arrNameJson = [];
      } else {
        //arrObjMember 배열의 0번째 요소는 가장 최근에 생성된 파일이 되도록 정렬
        arrObjMember.sort((a, b) => b.timeCreated - a.timeCreated);
        var arrNameJson = arrObjMember.map( nameFileJson => {
            return nameFileJson.nameFile;
          }
        )
        var listMemberLastest = JSON.parse(fs.readFileSync(directoryJsonMember + arrObjMember[0].nameFile, 'utf8')).member;
      }
          
      //길드원 목록을 json으로 저장
      setArrNameMember().then(result => {
        var titleKorean = getYmd("korean");
        var titleDash = getYmd("dash");
        //추가된 멤버 (신규 멤버 - 기존 멤버 차집합)
        const memberAdd = result.filter( nameMember => !listMemberLastest.includes(nameMember) );
        //제외된 멤버 (기존 멤버 - 신규 멤버 차집합)
        const memberOut = listMemberLastest.filter( nameMember => !result.includes(nameMember) );
        //추가된 멤버나 제외된 멤버가 있을 경우에만 .json 작성 (갱신)
        if ( memberAdd.length>0 || memberOut.length>0 ) {
          //memberYYYYMMDD.json에 들어갈 내용 작성
          var contentObjMemberResult = {
            "titleKorean":titleKorean,
            "url":titleDash,
            "member":result,
            "memberAdd":memberAdd,
            "memberOut":memberOut,
            "lengthMember":result.length,
            "lengthMemberAdd":memberAdd.length,
            "lengthMemberOut":memberOut.length
          };
          console.log(contentObjMemberResult);
          // 작성 실행
          fs.writeFile(`${directoryJsonMember}member${getYmd()}.json`, JSON.stringify(contentObjMemberResult), (err)=>{
            if (err) {
              throw err;
            }
            console.log("write end");
          })
          //map.json에 들어갈 내용 (작성)
          arrNameJson.unshift(`member${getYmd()}.json`);
          const contentNameFileMap = {
            "arrNameJson": arrNameJson,
            "nameLastestJson": arrNameJson[0]
          }
          fs.writeFile(directoryJsonMap, JSON.stringify(contentNameFileMap), (err) => {
            if (err) {
              throw err;
            }
          })
        };
      });
    }
  })
}
  
//한국시간 AM 04시 30분으로 설정됨 (UTC 19:30)
const batchMember = schedule.scheduleJob('0 30 19 * * *', writeJsonMember);

module.exports = batchMember;