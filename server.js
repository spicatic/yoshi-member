const modeDev = true;
const cors = require('cors');
const express = require("express");
const app = express();
const fs = require("fs");
const path = require("path");

const batchMember = require("./src/batchMember.js");

let port = 9001;

if ( modeDev ) {
  //외부 url에서 호출 시 CORS 오류 해결
  app.use(cors());
} else {
  port = 8080;
}

app.use('/', express.static(__dirname + '/build'));

app.use('/public', express.static(__dirname + '/public'));

app.get("*", function (req, res) {
  res.sendFile(path.join(__dirname, "./build/index.html"));
});

app.listen(port, () => {
  console.log(`Yoshi Member Back-End listening on port ${port}`);
});